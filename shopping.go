package main

import (
    "fmt"
    "strconv"
    "strings"
)

// Cart item interface, handles products and product packages, Command pattern
type CartItem interface {
    getName() string
    getPrice() float64
}

// Product class
type Product struct {
    name string
    price float64
}

func (p *Product) getName() string {
    return p.name
}

func (p *Product) getPrice() float64 {
    return p.price
}

// Product package class. A product package is composed out of several products
type ProductPackage struct {
    name string
    products []Product
}

func (pack *ProductPackage) addProduct(p *Product) {
    pack.products = append(pack.products, *p)
}

func (pack *ProductPackage) getName() string {
    return pack.name
}

func (pack *ProductPackage) getPrice() float64 {
    total := 0.00
    for i := 0; i < len(pack.products); i++ {
        total = total + pack.products[i].getPrice()
    }
    return total
}

// Observer for the discount application event
type DiscountObserver interface {
    onApplyDiscount(*Cart)
}

// The actual object
type Discount struct {
    value float64
}

func (d *Discount) onApplyDiscount(cart *Cart) {
    cart.discount = cart.discount + d.value
}

// Cart class, facade for calculating price
type Cart struct {
    cartItems []CartItem
    discount float64
    discountEventSubscribers []DiscountObserver
}

func (cart *Cart) getPrice() float64 {
    total := 0.00
    for i := 0; i < len(cart.cartItems); i++ {
        total = total + cart.cartItems[i].getPrice()
    }
    cart.applyDiscount()
    return total * (1 + cart.discount / 100)
}

func (cart *Cart) subscribeDiscount (d DiscountObserver) {
    cart.discountEventSubscribers = append(cart.discountEventSubscribers, d)
}

func (cart *Cart) applyDiscount() {
    for _, d := range cart.discountEventSubscribers {
        d.onApplyDiscount(cart)
    }
}

func (cart *Cart) printReceipt() {
    fmt.Println("Your receipt:")
    for i := 0; i < len(cart.cartItems); i++ {
        fmt.Println(cart.cartItems[i].getName())
        fmt.Println(strconv.FormatFloat(cart.cartItems[i].getPrice(), 'f', 2, 64))
    }
    fmt.Println(strings.Repeat("-", 10))
    fmt.Println("Total: " + strconv.FormatFloat(cart.getPrice(), 'f', 2, 64))
    fmt.Println("Discount: " + strconv.FormatFloat(cart.discount, 'f', 2, 64) + "%")
}

func main() {
    p1 := &Product{name: "Ursus Beer", price: 5.00};
    p2 := &Product{name: "Timisoreana Beer", price: 6.00}
    p3 := &Product{name: "Stela Artois Beer", price: 5.00}
    stelaSixPack := new(ProductPackage)
    stelaSixPack.name = "Stela Artois six pack"
    for i := 0; i < 6; i++ {
        stelaSixPack.addProduct(p3)
    }

    cart := Cart{cartItems: []CartItem{p1, p2, stelaSixPack}}
    discount := &Discount{value: -10.00}
    cart.subscribeDiscount(discount)

    cart.printReceipt()
}